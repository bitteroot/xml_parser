def get_raw_tags(xml):
    """
    Collect all the tags in order to return a list of lists
    [ 'tag1', 'tag2', 'tag2', 'tag1' ]
    """
    tags = []
    t = None
    for i in range(len(xml)):
        if xml[i] == '<':
            t = []
        elif xml[i] != '/' and xml[i] != '>':
            t.append(xml[i])
        elif xml[i] == '>':
            tags.append("".join(t))
    return tags


def is_valid(tags):
    """
    Validation all tags by finding it's matching pair from the outside in.
    """
    # missing tag case
    if len(tags) % 2 != 0:
        return False

    # non matching tags
    for i in range(len(tags)):
        if tags[i] != tags[-i - 1]:
            return False
    return True

if __name__ == '__main__':
    data = get_raw_tags("<tag><dog>abcd</dog></tag>")    # True
    # data = get_raw_tags("<tag><dog>abcd</tag></dog>")  #  False
    print is_valid(data)
